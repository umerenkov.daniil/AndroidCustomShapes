package daniil.umerenkov.pathplayground;

import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;

public class Shape {

    public Paint paint;
    public Path path;

    public Shape() {
        paint = new Paint();
        paint.setColor(Color.BLUE);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setStrokeWidth(3);

        path = new Path();
    }

    public void setCircle(float x, float y, float radius, Path.Direction dir) {
        path.reset();
        path.addCircle(x, y, radius, dir);
    }

    public void setPolygon(float x, float y, float radius, int numOfPt) {
        double section = 2.0 * Math.PI / numOfPt; //Количество радиан в сегменте
        path.reset();

        path.moveTo((float) (x + radius * Math.cos(0)), (float) (y + radius * Math.sin(0)));

        for (int i = 0; i < numOfPt; i++) {
            path.lineTo((float) (x + radius * Math.cos(section * i)), (float) (y + radius * Math.sin(section * i)));
        }
        path.close();
    }

    public void setStar(float x, float y, float radius, float innerRadius, int numOfPt) {
        double section = 2.0 * Math.PI / numOfPt;
        path.reset();

        path.moveTo((float) (x + radius * Math.cos(0)), (float) (y + radius * Math.sin(0)));
        path.lineTo((float) (x + innerRadius * Math.cos(section / 2.0)), (float) (y + innerRadius * Math.sin(section / 2.0)));

        for (int i = 0; i < numOfPt; i++) {
            path.lineTo((float) (x + radius * Math.cos(section * i)), (float) (y + radius * Math.sin(section * i)));
            path.lineTo((float) (x + innerRadius * Math.cos(section * i + section / 2.0)), (float) (y + innerRadius * Math.sin(section * i + section / 2.0)));
        }
        path.close();
    }
}
