package daniil.umerenkov.pathplayground;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.DiscretePathEffect;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;

import java.util.Calendar;
import java.util.stream.IntStream;

public class PathView extends View {

    private Shape _shape;

    public float ratioRadius;
    public float ratioInnerRadius;
    public int numberOfPoints = 3;
    public ShapeType shapeType;
    public float rotate;


    public PathView(Context context) {
        super(context);
        _init();
    }

    public PathView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        _init();
    }

    public PathView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        _init();
    }

    private void _init() {

        _shape = new Shape();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        int width = getWidth();
        int height = getHeight();
        if ((width == 0) || (height == 0)) {
            return;
        }

        float x = (float) width / 2.0f;
        float y = (float) height / 2.0f;
        float radius, innerRadius;

        radius = (width > height) ? height : width;
        innerRadius = radius;
        radius *= ratioRadius;
        innerRadius *= ratioInnerRadius;

        switch (shapeType) {
            case CIRCLE:
                _shape.setCircle(x, y, radius, Path.Direction.CCW);
                break;
            case POLYGON:
                _shape.setPolygon(x, y, radius, numberOfPoints);
                break;
            case STAR:
                _shape.setStar(x, y, radius, innerRadius, numberOfPoints);
                break;
        }

        canvas.save();
        canvas.rotate(rotate, x, y);

        canvas.drawPath(_shape.path, _shape.paint);
        canvas.restore();
    }

    public void reset() {
        _shape.path.reset();
    }

    public enum ShapeType {
        CIRCLE,
        POLYGON,
        STAR
    }
}