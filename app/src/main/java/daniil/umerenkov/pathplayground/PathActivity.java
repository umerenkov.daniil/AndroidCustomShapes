package daniil.umerenkov.pathplayground;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ComposePathEffect;
import android.graphics.CornerPathEffect;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathDashPathEffect;
import android.graphics.PathEffect;
import android.graphics.RectF;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class PathActivity extends AppCompatActivity {

    private static final int MIN_POINTS = 3;

    private SeekBar _seekBarRadius;
    private SeekBar _seekBarPoints;
    private SeekBar _seekBarInnerRadius;
    private SeekBar _seekBarRotate;
    private TextView _textViewRadius;
    private TextView _textViewInnerRadius;
    private TextView _textViewPoints;
    private TextView _textViewRotate;

    private PathView _pathView;
    private PathView.ShapeType _shapeType = PathView.ShapeType.CIRCLE;
    private RadioGroup _radioGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_path);

        _populateViews();

        _pathView.ratioRadius = (float) _seekBarRadius.getProgress() / (float) _seekBarRadius.getMax();
        _pathView.ratioInnerRadius = (float) _seekBarInnerRadius.getProgress() / (float) _seekBarInnerRadius.getMax();
        _updateShape();

        _seekBarRadius.setOnSeekBarChangeListener(onSeekBarChangeListener);
        _seekBarPoints.setOnSeekBarChangeListener(onSeekBarPointChangeListener);
        _seekBarInnerRadius.setOnSeekBarChangeListener(onSeekBarInnerChangeListener);
        _radioGroup.setOnCheckedChangeListener(onCheckedChangeListener);
        _seekBarRotate.setOnSeekBarChangeListener(onSeekBarRotateChangeListener);
    }

    private void _populateViews() {
        _seekBarRadius = (SeekBar) findViewById(R.id.sbRadius);
        _seekBarPoints = (SeekBar) findViewById(R.id.sbPoints);
        _seekBarInnerRadius = (SeekBar) findViewById(R.id.sbInnerRadius);
        _seekBarRotate = (SeekBar) findViewById(R.id.sbRotate);
        _textViewRadius = (TextView) findViewById(R.id.textViewRadius);
        _textViewInnerRadius = (TextView) findViewById(R.id.textViewInnerRadius);
        _textViewPoints = (TextView) findViewById(R.id.textViewPoints);
        _textViewRotate = (TextView) findViewById(R.id.textViewRotate);

        _pathView = (PathView) findViewById(R.id.PathView);
        _radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
    }

    RadioGroup.OnCheckedChangeListener onCheckedChangeListener = new RadioGroup.OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(RadioGroup group, int checkedId) {
            switch (checkedId) {
                case R.id.radioCircle:
                    _shapeType = PathView.ShapeType.CIRCLE;
                    break;
                case R.id.radioPolygon:
                    _shapeType = PathView.ShapeType.POLYGON;
                    break;
                case R.id.radioStar:
                    _shapeType = PathView.ShapeType.STAR;
                    break;
            }
            _updateShape();
        }
    };

    private void _updateShape() {
        _pathView.shapeType = _shapeType;
        View[] allViews = new View[]{
                _seekBarPoints, _seekBarRadius, _seekBarInnerRadius,
                _textViewPoints, _textViewInnerRadius, _textViewRadius};

        _setVisibility(allViews, false);

        switch (_shapeType) {
            case CIRCLE:
                _setVisibility(new View[]{_seekBarRadius, _textViewRadius}, true);
                break;
            case POLYGON:
                _setVisibility(new View[]{_seekBarRadius, _textViewRadius, _seekBarPoints, _textViewPoints}, true);
                break;
            case STAR:
                _setVisibility(allViews, true);
                break;
        }

        _pathView.invalidate();
    }

    private void _setVisibility(View[] views, boolean flag) {
        int value = flag ? View.VISIBLE : View.INVISIBLE;
        for (View view : views) {
            view.setVisibility(value);
        }
    }

    SeekBar.OnSeekBarChangeListener onSeekBarRotateChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            _pathView.rotate = progress - 180;
            _pathView.invalidate();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            _pathView.ratioRadius = (float) _seekBarRadius.getProgress() / (float) _seekBarRadius.getMax();
            _pathView.invalidate();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarPointChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            _pathView.numberOfPoints = progress + MIN_POINTS;
            _pathView.invalidate();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };

    SeekBar.OnSeekBarChangeListener onSeekBarInnerChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            _pathView.ratioInnerRadius = (float) _seekBarInnerRadius.getProgress() / (float) _seekBarInnerRadius.getMax();
            _pathView.invalidate();
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
}
